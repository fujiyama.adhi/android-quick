package com.example.ict.tracking_2.Adapter;

/**
 * Created by trial on 12/19/17.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ict.tracking_2.R;

import java.util.ArrayList;

/**
 * Created by alex on 5/6/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    final private ItemClickListener mClickListener;
    private ArrayList<String> mReference, mProduk;

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

    public RecyclerViewAdapter(ItemClickListener mClickListener, ArrayList<String> Reference, ArrayList<String> Produk) {
        this.mClickListener = mClickListener;
        this.mProduk = Produk;
        this.mReference = Reference;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.view_rv_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_reference.setText(mReference.get(position));
        holder.tv_produk.setText(mProduk.get(position));
    }

    @Override
    public int getItemCount() {
        return mReference.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView tv_produk;
        private TextView tv_reference;
        CardView cv_root;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_reference = (TextView)itemView.findViewById(R.id.tvlist_id_order);
            tv_produk = (TextView)itemView.findViewById(R.id.tvlist_id_reference);
            cv_root = (CardView)itemView.findViewById(R.id.card_view);
           /* itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);*/
            cv_root.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }


}

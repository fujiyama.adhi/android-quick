package com.example.ict.tracking_2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ict.tracking_2.Adapter.RecyclerViewAdapter;
import com.example.ict.tracking_2.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.example.ict.tracking_2.LoginActivity.isNetworkAvailable;

public class ListActivity extends AppCompatActivity implements
        RecyclerViewAdapter.ItemClickListener {
    String myJSON, id_kurir, nama_kurir, username, password;
    ArrayList<String> id_orderA, referenceA, id_pelangganA, firstnameA, lastnameA,
            cust_addressA, cust_postcodeA, cust_cityA, cust_phoneA,
            cust_phone_mobileA, product_nameA, product_quantityA;
    ArrayList<Double> latitudeA, longitudeA;
    FloatingActionButton fb_keranjang;
    JSONArray result1 = null;
    SharedPrefManager SP_Help;
    TextView tv_nama;

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mmAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        SP_Help = new SharedPrefManager(this);
        id_kurir = SP_Help.getSPID();
        nama_kurir = SP_Help.getSPNama();
        username = SP_Help.getSPUsername();
        password = SP_Help.getSPPassword();

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_main);
        tv_nama = (TextView) findViewById(R.id.tv_nama_akun);
        fb_keranjang = (FloatingActionButton) findViewById(R.id.fb_keranjang);

        fb_keranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListActivity.this, KeranjangActivity.class);
                startActivity(i);
                overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
                finish();
            }
        });

        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"No Internet connection",Toast.LENGTH_LONG).show();
             //Calling this method to close this activity when internet is not available.
        } else {
            getData();
        }

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);

        tv_nama.setText(nama_kurir);
    }

    protected void onResume(){
        super.onResume();
        getData();
    }

    public void getData() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost(Config.FILE_GET);

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                } finally {
                    try {
                        if (inputStream != null) inputStream.close();
                    } catch (Exception squish) {
                    }
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                myJSON = result;
                save_object();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (isLongClick) {
            Toast.makeText(getApplicationContext(), "#" + position + " - " + id_orderA.get(position), Toast.LENGTH_SHORT).show();
        }
        else {

            Intent intent = new Intent(ListActivity.this , DetailActivity.class);
            Bundle c = new Bundle();

            c.putString("id_order", id_orderA.get(position));
            c.putString("reference", referenceA.get(position));
            c.putString("id_pelanggan", id_pelangganA.get(position));
            c.putString("firstname", firstnameA.get(position));
            c.putString("lastname", lastnameA.get(position));
            c.putString("cust_address", cust_addressA.get(position));
            c.putString("cust_postcode", cust_postcodeA.get(position));
            c.putString("cust_city", cust_cityA.get(position));
            c.putString("cust_phone", cust_phoneA.get(position));
            c.putString("cust_phone_mobile", cust_phone_mobileA.get(position));
            c.putString("product_name", product_nameA.get(position));
            c.putString("product_quantity", product_quantityA.get(position));
            c.putDouble("latitude", latitudeA.get(position));
            c.putDouble("longitude", longitudeA.get(position));

            intent.putExtras(c);
            startActivity(intent);
            overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
            finish();
        }
    }

    protected void save_object() {
        id_orderA = new ArrayList<String>();
        referenceA =  new ArrayList<String>();
        firstnameA = new ArrayList<String>();
        lastnameA = new ArrayList<String>();
        cust_addressA = new ArrayList<String>();
        cust_postcodeA = new ArrayList<String>();
        cust_cityA = new ArrayList<String>();
        cust_phoneA = new ArrayList<String>();
        cust_phone_mobileA = new ArrayList<String>();
        product_nameA = new ArrayList<String>();
        product_quantityA = new ArrayList<String>();
        id_pelangganA = new ArrayList<String>();
        latitudeA = new ArrayList<Double>();
        longitudeA = new ArrayList<Double>();


        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            result1 = jsonObj.getJSONArray("result");

            for (int i = 0; i < result1.length(); i++) {
                JSONObject c = result1.getJSONObject(i);

                id_orderA.add(c.getString("id_order"));
                referenceA.add(c.getString("reference"));
                firstnameA.add(c.getString("firstname"));
                lastnameA.add(c.getString("lastname"));
                cust_addressA.add(c.getString("cust_address"));
                cust_postcodeA.add(c.getString("cust_postcode"));
                cust_cityA.add(c.getString("cust_city"));
                cust_phoneA.add(c.getString("cust_phone"));
                cust_phone_mobileA.add(c.getString("cust_phone_mobile"));
                product_nameA.add(c.getString("product_name"));
                product_quantityA.add(c.getString("product_quantity"));
                id_pelangganA.add(c.getString("id_pelanggan"));
                latitudeA.add(c.getDouble("latitude"));
                longitudeA.add(c.getDouble("longitude"));
            }

            if (id_orderA != null){
                mmAdapter = new RecyclerViewAdapter(this, referenceA, product_nameA);
                mRecyclerView.setAdapter(mmAdapter);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void layout_akun(View view){
        Intent i = new Intent(ListActivity.this, AkunActivity.class);

        startActivity(i);
        overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
        finish();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}

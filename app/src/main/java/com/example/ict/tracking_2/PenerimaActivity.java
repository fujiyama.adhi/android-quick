package com.example.ict.tracking_2;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.example.ict.tracking_2.Config.Config;
import com.example.ict.tracking_2.Config.ConnectMySql;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PenerimaActivity extends AppCompatActivity {
    ArrayList<String> list_kirim;
    String id_order, nama_penerima, nomor_penerima, namaCus, latitude, longitude, id_kirim;
    EditText et_penerima, et_noPenerima;
    SwipeButton btn_swipe;
    InputStream is = null;
    String result = null;
    String line = null;

    Connection mConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penerima);

        allowNetworkOnMainThread();
        mConn = new ConnectMySql().getConnection();

        Bundle b = this.getIntent().getExtras();
//        list_kirim = b.getStringArrayList("list_kirim");
        id_kirim = b.getString("id_kirim");
        id_order = b.getString("id_order");
        namaCus = b.getString("namaCus");
        latitude = b.getString("latitude");
        longitude = b.getString("longitude");

        et_penerima = (EditText) findViewById(R.id.et_penerima);
        et_noPenerima = (EditText) findViewById(R.id.et_noPenerima);
        btn_swipe = (SwipeButton) findViewById(R.id.btn_swipe);

        btn_swipe.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                nama_penerima = et_penerima.getText().toString();
                nomor_penerima = et_noPenerima.getText().toString();

                if (active){
                    if (nama_penerima.isEmpty() || nomor_penerima.isEmpty()){
                        Toast.makeText(PenerimaActivity.this, "Kosong cok", Toast.LENGTH_LONG).show();
                    }
                    else {
                        updateStatus(id_kirim);
                        insertStatus();
//            insertKeterima();
                    }
                }
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    void updateStatus(String id_kirim){
        try {
            Statement statement = mConn.createStatement();
            String mQuery = "UPDATE ps_kirim SET \n"+
                    "status = '2'\n" +
                    "WHERE id_kirim= '"+id_kirim+"'";
            Log.d ("Query ",mQuery);
            int rowAffect = statement.executeUpdate(mQuery);
            Log.d ("Execute", rowAffect+"rowAffected");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void insertStatus() {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("id_order", id_order));
        nameValuePairs.add(new BasicNameValuePair("status","2"));
        nameValuePairs.add(new BasicNameValuePair("nama_pembeli",namaCus));
        nameValuePairs.add(new BasicNameValuePair("nama_penerima",nama_penerima));
        nameValuePairs.add(new BasicNameValuePair("no_telp",nomor_penerima));
        nameValuePairs.add(new BasicNameValuePair("latitude",latitude));
        nameValuePairs.add(new BasicNameValuePair("longitude",longitude));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.DONE);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());

        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
//            list_kirim.add(result);
            Log.e("pass 2", "connection status : " + result);
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }
        Log.e("Result insert : ", result);
        try
        {

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }

        if (result.length() > 10){
        }
        else {
            Toast.makeText(this, "Barang sudah diterima", Toast.LENGTH_LONG).show();

            Intent i = new Intent(PenerimaActivity.this, SuccessActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}

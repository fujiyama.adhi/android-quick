package com.example.ict.tracking_2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ict.tracking_2.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    String result_login, id_kurir, nama_kurir, username, password;
    EditText et_username, et_password;
    SharedPrefManager SP_Helper;
    JSONArray result1 = null;
    String line = null;
    Spinner spinner;
    Button bt_login;
    View parentView;
    TextView get;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SP_Helper = new SharedPrefManager(this);

        get = (TextView) findViewById(R.id.get);
        parentView = findViewById(R.id.parentView);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        spinner = (Spinner) findViewById(R.id.spinner_id);

        if (SP_Helper.getSPSudahLogin()){
            startActivity(new Intent(LoginActivity.this, ListActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
        bt_login = (Button) findViewById(R.id.bt_start);

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Inform the user the button has been clicked
                username = et_username.getText().toString();
                password = et_password.getText().toString();

                if (username.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Silahkan isi Username dan Password", Toast.LENGTH_SHORT).show();
                    Log.d("Username: ", "Kosong");
                }
                else {
                    login();
                }
            }
        });

        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"No Internet connection",Toast.LENGTH_LONG).show();
            finish(); //Calling this method to close this activity when internet is not available.
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }

    public void login() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("username",String.valueOf(username)));
                nameValuePairs.add(new BasicNameValuePair("password",String.valueOf(password)));

                InputStream inputStream = null;
                String result = null;
                try
                {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.LOGIN);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();

                    Log.e("pass 1", "connection success ");
                }
                catch(Exception e)
                {
                    Log.e("Fail 1", e.toString());

                }

                try
                {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(inputStream,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result = sb.toString();
                    result_login = result;
                    Log.e("pass 2", "connection success : " + result);
                }
                catch(Exception e)
                {
                    Log.e("Fail 2", e.toString());
                }

                Log.e("Result", result);
                try
                {

                }
                catch(Exception e)
                {
                    Log.e("Fail 3", e.toString());
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                result_login = result;
                action();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    protected void action() {
        String c = Character.toString(result_login.charAt(0));
        if (c.equals("0")){
            Toast.makeText(getApplicationContext(), "Akun atau Password yang anda masukkan salah", Toast.LENGTH_SHORT).show();
        }
        else {

            try {
                JSONObject jsonObj = new JSONObject(result_login);
                result1 = jsonObj.getJSONArray("result");
                JSONObject u = result1.getJSONObject(0);
                id_kurir = u.getString("id_kurir");
                nama_kurir = u.getString("nama_kurir");
                username = u.getString("username_kurir");
                password = u.getString("password_kurir");

                SP_Helper.saveSPString(SharedPrefManager.SP_USERNAME, username);
                SP_Helper.saveSPString(SharedPrefManager.SP_ID, id_kurir);
                SP_Helper.saveSPString(SharedPrefManager.SP_PASSWORD, password);
                SP_Helper.saveSPString(SharedPrefManager.SP_NAMA, nama_kurir);
                SP_Helper.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);

                Intent i = new Intent(LoginActivity.this, ListActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                Bundle bundle = new Bundle();
                bundle.putString("id_kurir",id_kurir);
                bundle.putString("nama_kurir",nama_kurir);
                bundle.putString("username",username);
                bundle.putString("password", password);

                i.putExtras(bundle);
                startActivity(i);
                overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
                finish();
                Toast.makeText(getApplicationContext(), nama_kurir, Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}

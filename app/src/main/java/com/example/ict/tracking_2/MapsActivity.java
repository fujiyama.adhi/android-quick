package com.example.ict.tracking_2;

import java.util.Arrays;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ict.tracking_2.Object.Jarak;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.ict.tracking_2.Config.ConnectMySql;
import com.google.maps.android.SphericalUtil;

public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    double latitude, longitude, lat_2, long_2, jarak;
    float blue = 240.0f, red = 0.0f, orange = 30.0f;
    TelephonyManager mTelephonyManager;
    GoogleApiClient mGoogleApiClient;
    Polyline poly;
    LocationRequest mLocationRequest;
    TextView Text_kode;
    Connection mConn;
    SharedPrefManager SP_Help;
    String id_kurir, imei_get;
    Marker mCurrLocationMarker, mDesLoc;
    Jarak[] jaraks;
    ArrayList<String> list_kirim, list_latDes, list_longDes;
    List<Jarak> pelanggan_jarak= new ArrayList<Jarak>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        allowNetworkOnMainThread();
        mConn = new ConnectMySql().getConnection();

        Bundle b = this.getIntent().getExtras();

        list_kirim = b.getStringArrayList("list_kirim");
        list_latDes = b.getStringArrayList("list_latDes");
        list_longDes = b.getStringArrayList("list_longDes");

        SP_Help = new SharedPrefManager(this);
        id_kurir = SP_Help.getSPID();

        Text_kode = (TextView) findViewById(R.id.text_status);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        Text_kode.setText("Jumlah barang : " + list_kirim.size());
//        getDeviceImei();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
    }

    public void onclick_arrived(View view) {
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        LatLng latLng = new LatLng(latitude, longitude);
        LatLng desty = new LatLng(lat_2,long_2);

        jarak = distance(latLng, desty);

        new AlertDialog.Builder(this)
                .setMessage("Sudah sampai ke tujuan?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(){
                            public void run(){
                                Intent i = new Intent(MapsActivity.this, AprovActivity.class);
                                Bundle c = new Bundle();

//                                c.putStringArrayList("list_kirim", list_kirim);
                                c.putString("id_kirim", pelanggan_jarak.get(0).getId_kirim());
                                c.putDouble("latitude", latitude);
                                c.putDouble("longitude", longitude);

                                i.putExtras(c);
                                startActivity(i);
                            }
                        }.start();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    private void getDeviceImei() {
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imei_get = mTelephonyManager.getDeviceId();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
            }
        }
        else {
            buildGoogleApiClient();
        }

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    Runnable r2=new Runnable() {
        @Override
        public void run() {
            //Your Toast
            Changed();
            h2.postDelayed(r2,10000);
        }
    };

    Handler h2=new Handler();


    public void onResume() {
        h2.postDelayed(r2,10000);
        super.onResume();
    }

    public void onPause() {
        h2.removeCallbacks(r2);
        super.onPause();
    }

    public void Changed() {
        getCurrentLocation();

        long_2 = pelanggan_jarak.get(0).getLng();
        lat_2 = pelanggan_jarak.get(0).getLat();

        LatLng latLng = new LatLng(latitude, longitude);
        LatLng desty = new LatLng(lat_2,long_2);

        mCurrLocationMarker = marker(latLng, true, "Kurir");

        distance(latLng, desty);

        if (poly == null){
            String url = getUrl(latLng, desty);
            FetchUrl FetchUrl = new FetchUrl();
            FetchUrl.execute(url);
        }

        if (mDesLoc == null){
            for (int i=0; i<list_kirim.size(); i++){
                LatLng alldes = new LatLng(pelanggan_jarak.get(i).getLat(),pelanggan_jarak.get(0).getLng());
                mDesLoc = marker(alldes, false, "Pemesan");
                upload_track(list_kirim.get(i), latitude, longitude);
            }
        }

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public  void getCurrentLocation(){
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //Place current location marker
        GPSTracker gps = new GPSTracker(this);
        if(gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }
    }

    public Double distance(LatLng from, LatLng to){
        Double distance = SphericalUtil.computeDistanceBetween(from, to);
//        Toast.makeText(this,String.valueOf(distance+" Meters"),Toast.LENGTH_SHORT).show();
        return distance;
    }

    public Marker marker(LatLng latLng, boolean mark_type, String subjek){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(subjek);
        if(mark_type){
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_kurir));
            markerOptions.anchor(0.5f, 1.0f);
        }
        else {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(red));
        }
        return mMap.addMarker(markerOptions);
    }

    @Override
    public void onLocationChanged(Location location) {
        getCurrentLocation();
        GPSTracker gps = new GPSTracker(this);
        jaraks = new Jarak[list_kirim.size()];
        for (int i=0; i<list_kirim.size(); i++){
            lat_2 = Double.parseDouble(list_latDes.get(i));
            long_2 = Double.parseDouble(list_longDes.get(i));
            jarak = distance(new LatLng(latitude,longitude), new LatLng(lat_2, long_2));
            jaraks[i]= new Jarak(list_kirim.get(i), lat_2, long_2, jarak);
        }
        Arrays.sort(jaraks, Jarak.PelangganJarakComparator);
        for (int i = 0; i<jaraks.length; i++){
            pelanggan_jarak.add(jaraks[i]);
        }

        if(gps.canGetLocation()) {
            mCurrLocationMarker = marker(new LatLng(latitude,longitude), true, "Kurir");
            CameraPosition newCamPos = new CameraPosition(new LatLng(latitude,longitude),
                    15.5f,
                    mMap.getCameraPosition().tilt, //use old tilt
                    mMap.getCameraPosition().bearing); //use old bearing
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 3000, null);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    public void upload_track(String id_kirim, double latitude, double longitude){

        try {
            Statement statement = mConn.createStatement();
            String mQuery = "UPDATE ps_kirim SET \n"+
                    "latitude = '"+latitude+"',\n"+
                    "longitude = '"+longitude+"'\n" +
                    "WHERE id_kirim= '"+id_kirim+"'";
            Log.d ("Query :",mQuery);
            int rowAffect = statement.executeUpdate(mQuery);
            Log.d ("Execute", rowAffect+"rowAffected");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Apakah anda ingin kembali ke keranjang?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MapsActivity.this, KeranjangActivity.class);

                        startActivity(i);
                        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(getResources().getColor(R.color.colorPrimarySmooth));

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if (poly != null){
                    poly.remove();
                    Log.d("Polyline :", "Remove");
                }
                poly = mMap.addPolyline(lineOptions);
                Log.d("Polyline :", "Tergambar");
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}
package com.example.ict.tracking_2.Object;

import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Created by trial on 1/11/18.
 */

public class Jarak implements Comparable<Jarak>{

    private String id_kirim;
    private double lat;
    private double lng;
    private double jarak;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getJarak() {
        return jarak;
    }

    public void setJarak(double jarak) {
        this.jarak = jarak;
    }

    public String getId_kirim() {

        return id_kirim;
    }

    public void setId_kirim(String id_kirim) {
        this.id_kirim = id_kirim;
    }

    public Jarak(String id_kirim, double lat, double lng, double jarak) {
        super();
        this.id_kirim = id_kirim;
        this.lat = lat;
        this.lng = lng;
        this.jarak = jarak;
    }

    @Override
    public int compareTo(@NonNull Jarak comparePelanggan) {
        Double compareJarak = ((Jarak) comparePelanggan).getJarak();
        return (int) (this.jarak - compareJarak);
    }

    public static Comparator<Jarak> PelangganJarakComparator
            = new Comparator<Jarak>() {

        public int compare(Jarak loc1, Jarak loc2) {

            Double PelJarak1 = loc1.getJarak();
            Double PelJarak2 = loc2.getJarak();

            //ascending order
            return PelJarak1.compareTo(PelJarak2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }
    };

}

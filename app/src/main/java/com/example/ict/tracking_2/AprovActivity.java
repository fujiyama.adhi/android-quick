package com.example.ict.tracking_2;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.example.ict.tracking_2.Config.ConnectMySql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AprovActivity extends AppCompatActivity {
    ArrayList<String> list_kirim;
    String namaCus, reference, namaProd, alamatCus, jumlahProd, id_order, latitude, longitude, id_kirim;

    TextView tv_cusname_aprov, tv_nama_produk_aprove, tv_reference_aprove,
            tv_jumlah_aprove, tv_alamat_aprove;

    Connection mConn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprov);

        allowNetworkOnMainThread();
        mConn = new ConnectMySql().getConnection();

        Bundle b = this.getIntent().getExtras();
//        list_kirim = b.getStringArrayList("list_kirim");
        id_kirim = b.getString("id_kirim");
        latitude = String.valueOf(b.getDouble("latitude"));
        longitude = String.valueOf(b.getDouble("longitude"));

        tv_cusname_aprov = (TextView) findViewById(R.id.tv_cusname_aprov);
        tv_nama_produk_aprove = (TextView) findViewById(R.id.tv_nama_produk_aprove);
        tv_reference_aprove = (TextView) findViewById(R.id.tv_reference_aprove);
        tv_jumlah_aprove = (TextView) findViewById(R.id.tv_jumlah_aprove);
        tv_alamat_aprove = (TextView) findViewById(R.id.tv_alamat_aprove);

        showOrder();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void onclick_aprov(View view) {
        Intent i = new Intent(AprovActivity.this, PenerimaActivity.class);
        Bundle c = new Bundle();

//        c.putStringArrayList("list_kirim", list_kirim);
        c.putString("id_kirim", id_kirim);
        c.putString("id_order", id_order);
        c.putString("namaCus", namaCus);
        c.putString("latitude", latitude);
        c.putString("longitude", longitude);

        i.putExtras(c);
        startActivity(i);
//        finish();
    }

    void showOrder(){
        try{Statement statement = mConn.createStatement();
            String mQuery = "SELECT psk.id_kirim, pso.reference, psod.product_name, " +
                    "psc.firstname, psc.lastname, psod.product_quantity, psa.address1, psa.city, psk.id_order\n" +
                    "FROM ps_kirim psk\n" +
                    "LEFT JOIN ps_orders pso ON psk.id_order = pso.id_order\n" +
                    "LEFT JOIN ps_order_detail psod ON psk.id_order = psod.id_order\n" +
                    "LEFT JOIN ps_address psa ON psa.id_address = pso.id_address_delivery\n" +
                    "LEFT JOIN ps_customer psc ON pso.id_customer = psc.id_customer\n" +
                    "WHERE psk.id_kirim = " +id_kirim;
            ResultSet result = statement.executeQuery(mQuery);
            while (result.next()){
                reference =result.getString(2);
                namaProd =result.getString(3);
                namaCus = result.getString(4) +" "+ result.getString(5);
                jumlahProd =result.getString(6);
                alamatCus =result.getString(7) +" "+ result.getString(8);
                id_order =result.getString(9);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        tv_cusname_aprov.setText(namaCus);
        tv_nama_produk_aprove.setText(namaProd);
        tv_reference_aprove.setText(reference);
        tv_jumlah_aprove.setText(jumlahProd);
        tv_alamat_aprove.setText(alamatCus);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}

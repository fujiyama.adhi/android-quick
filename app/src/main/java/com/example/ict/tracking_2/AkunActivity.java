package com.example.ict.tracking_2;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class AkunActivity extends AppCompatActivity {
    String id_kurir, nama_kurir, username;
    TextView tv_id_kurir, tv_username, tv_namalengkap;

    SharedPrefManager SP_Help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akun);

        SP_Help = new SharedPrefManager(this);
        id_kurir = SP_Help.getSPID();
        nama_kurir = SP_Help.getSPNama();
        username = SP_Help.getSPUsername();

        tv_id_kurir = (TextView) findViewById(R.id.tv_id_kurir);
        tv_namalengkap = (TextView) findViewById(R.id.tv_nama_lengkap);
        tv_username = (TextView) findViewById(R.id.tv_username);

        tv_id_kurir.setText(id_kurir);
        tv_username.setText(username);
        tv_namalengkap.setText(nama_kurir);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void oc_logout(View view) {
        SP_Help.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
        startActivity(new Intent(AkunActivity.this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Intent i = new Intent(AkunActivity.this, ListActivity.class);

            startActivity(i);
            overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(AkunActivity.this, ListActivity.class);

        startActivity(i);
        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        finish();
    }

}

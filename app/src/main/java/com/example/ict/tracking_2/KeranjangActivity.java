package com.example.ict.tracking_2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ict.tracking_2.Adapter.RecyclerViewAdapter;
import com.example.ict.tracking_2.Config.ConnectMySql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class KeranjangActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {
    String id_kurir;
    ArrayList<String> list_id_kirim, list_id_order, list_reference, list_product_name,
            list_latDes, list_longDes, list_name;

    SharedPrefManager SP_Help;
    ImageView image_cart;
    Connection mConn;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mmAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);

        SP_Help = new SharedPrefManager(this);
        id_kurir = SP_Help.getSPID();
        allowNetworkOnMainThread();

        image_cart = (ImageView) findViewById(R.id.image_cart);
        image_cart.setAlpha(75);

        mConn = new ConnectMySql().getConnection();

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_keranjang);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        showKeranjang();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    protected void onResume(){
        super.onResume();
        showKeranjang();
    }

    public void onclik_antarkeranjang(View view) {
        if (list_id_kirim.size() >= 1){
            new AlertDialog.Builder(this)
                    .setMessage("Apakah anda yakin dengan pesanan yang akan anda antar?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new Thread(){
                                public void run(){
                                    Intent i = new Intent(KeranjangActivity.this, MapsActivity.class);
                                    Bundle c = new Bundle();

                                    c.putStringArrayList("list_kirim", list_id_kirim);
                                    c.putStringArrayList("list_latDes", list_latDes);
                                    c.putStringArrayList("list_longDes", list_longDes);

                                    i.putExtras(c);
                                    startActivity(i);
                                    overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
                                    finish();
                                }
                            }.start();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        }
        else {
            Toast.makeText(getApplicationContext(), "Keranjang kosong", Toast.LENGTH_SHORT).show();
        }
    }

    public void onclick_tambah(View view) {
        Intent i = new Intent(KeranjangActivity.this, ListActivity.class);

        startActivity(i);
        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        finish();
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (isLongClick) {
        }
        else {
            deleteData(list_id_kirim.get(position), list_product_name.get(position));
            showKeranjang();
        }
    }

    void showKeranjang(){
        list_product_name = new ArrayList<String>();
        list_reference = new ArrayList<String>();
        list_id_kirim = new ArrayList<String>();
        list_name = new ArrayList<String>();
        list_latDes = new ArrayList<String>();
        list_longDes = new ArrayList<String>();
        list_id_order = new ArrayList<String>();

        try{Statement statement = mConn.createStatement();
            String mQuery = "SELECT psk.id_kirim, pso.reference, psod.product_name, psa.latitude, " +
                    "psa.longitude, psc.firstname, psc.lastname, psk.id_order\n" +
                    "FROM ps_kirim psk\n" +
                    "LEFT JOIN ps_orders pso ON psk.id_order = pso.id_order\n" +
                    "LEFT JOIN ps_order_detail psod ON psk.id_order = psod.id_order\n" +
                    "LEFT JOIN ps_address psa ON psa.id_address = pso.id_address_delivery\n" +
                    "LEFT JOIN ps_customer psc ON pso.id_customer = psc.id_customer\n" +
                    "WHERE psk.id_kurir = " +id_kurir+ " AND psk.status = 1";
            ResultSet result = statement.executeQuery(mQuery);
            while (result.next()){
                list_id_kirim.add(result.getString(1));
                list_reference.add(result.getString(2));
                list_product_name.add(result.getString(3));
                list_latDes.add(result.getString(4));
                list_longDes.add(result.getString(5));
                list_name.add(result.getString(6) +" "+ result.getString(7));
                list_id_order.add(result.getString(8));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        if (list_reference != null){
            mmAdapter = new RecyclerViewAdapter(this, list_reference, list_product_name);
            mRecyclerView.setAdapter(mmAdapter);
        }

        if (list_reference.size() >= 1){
            image_cart.setVisibility(View.GONE);
        }
        else {
            image_cart.setVisibility(View.VISIBLE);
        }

        Log.d("ID_KIRIM", String.valueOf(list_id_kirim.size()));
    }

    void deleteData(String id_kirim, String product_name) {

        try {
            Statement statement = mConn.createStatement();
            String mQuery = "DELETE FROM ps_kirim WHERE id_kirim= '"+id_kirim+"'";
            Log.d ("Query :",mQuery);
            int rowAffect = statement.executeUpdate(mQuery);
            Log.d ("Execute", rowAffect+"rowAffected");
        }catch (SQLException e){
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), product_name+" Telah di hapus", Toast.LENGTH_SHORT).show();
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Intent i = new Intent(KeranjangActivity.this, ListActivity.class);

            startActivity(i);
            overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(KeranjangActivity.this, ListActivity.class);

        startActivity(i);
        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        finish();
    }
}

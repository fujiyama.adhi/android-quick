package com.example.ict.tracking_2;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ict.tracking_2.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {
    String id_order, reference, id_pelanggan, firstname, lastname, cust_address,
            cust_postcode, cust_city, cust_phone, cust_phone_mobile,
            product_name, product_quantity, latitude = "0", longitude = "0", status = "1",
            id_kurir;
    TextView tv_id_order, tv_reference, tv_nama_produk, tv_jumlah,
            tv_nama, tv_alamat, tv_kode_pos, tv_kota, tv_no_telf,
            tv_no_hp;
    double lat_des, long_des;
    ProgressBar pb_addKeranjang;
    InputStream is = null;
    String result = null;
    String line = null;

    SharedPrefManager SP_Help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        allowNetworkOnMainThread();

        SP_Help = new SharedPrefManager(this);
        Bundle b = this.getIntent().getExtras();
        id_order = b.getString("id_order");
        reference = b.getString("reference");
        id_pelanggan = b.getString("id_pelanggan");
        firstname = b.getString("firstname");
        lastname = b.getString("lastname");
        cust_address = b.getString("cust_address");
        cust_postcode = b.getString("cust_postcode");
        cust_city = b.getString("cust_city");
        cust_phone = b.getString("cust_phone");
        cust_phone_mobile = b.getString("cust_phone_mobile");
        product_name = b.getString("product_name");
        product_quantity = b.getString("product_quantity");
        lat_des = b.getDouble("latitude");
        long_des = b.getDouble("longitude");

        id_kurir = SP_Help.getSPID();

        pb_addKeranjang = (ProgressBar) findViewById(R.id.pb_addKeranjang);

        setContent();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void setContent(){
        tv_id_order = (TextView) findViewById(R.id.tv_id_order);
        tv_reference = (TextView) findViewById(R.id.tv_reference);
        tv_nama_produk = (TextView) findViewById(R.id.tv_nama_produk);
        tv_jumlah = (TextView) findViewById(R.id.tv_jumlah);
        tv_nama = (TextView) findViewById(R.id.tv_nama);
        tv_alamat = (TextView) findViewById(R.id.tv_alamat);
        tv_kode_pos = (TextView) findViewById(R.id.tv_kode_pos);
        tv_kota = (TextView) findViewById(R.id.tv_kota);
        tv_no_telf = (TextView) findViewById(R.id.tv_no_telf);
        tv_no_hp = (TextView) findViewById(R.id.tv_no_hp);

        tv_id_order.setText(id_order);
        tv_reference.setText(reference);
        tv_nama_produk.setText(product_name);
        tv_jumlah.setText(product_quantity);
        tv_nama.setText(firstname + " " + lastname);
        tv_alamat.setText(cust_address);
        tv_kode_pos.setText(cust_postcode);
        tv_kota.setText(cust_city);
        tv_no_telf.setText(cust_phone);
        tv_no_hp.setText(cust_phone_mobile);
    }

    public void onclik_keranjang(View view) {
        pb_addKeranjang.setVisibility(View.VISIBLE);

        insertData();
    }

    public void insertData() {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("id_kurir",id_kurir));
        nameValuePairs.add(new BasicNameValuePair("id_order",id_order));
        nameValuePairs.add(new BasicNameValuePair("id_pelanggan",id_pelanggan));
        nameValuePairs.add(new BasicNameValuePair("latitude",String.valueOf(latitude)));
        nameValuePairs.add(new BasicNameValuePair("longitude",String.valueOf(longitude)));
        nameValuePairs.add(new BasicNameValuePair("status",String.valueOf(status)));

        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.INSERT);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString());

        }

        try
        {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success insert : " + result);
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        }
        Log.e("Result insert :", result);
        try
        {

        }
        catch(Exception e)
        {
            Log.e("Fail 3", e.toString());
        }

        if (result.length() > 10){
            Toast.makeText(getApplicationContext(), "Barang sudah ada", Toast.LENGTH_SHORT).show();
            pb_addKeranjang.setVisibility(View.GONE);
        }
        else {
            Intent i = new Intent(DetailActivity.this, KeranjangActivity.class);

            startActivity(i);
            overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left);
            finish();
        }

    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Intent i = new Intent(DetailActivity.this, ListActivity.class);

            startActivity(i);
            overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(DetailActivity.this, ListActivity.class);

        startActivity(i);
        overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        finish();
    }
}

package com.example.ict.tracking_2.Config;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by trial on 12/20/17.
 */

public class ConnectMySql {
    Connection conn = null;
    public Connection getConnection() {


        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String cUrl = "jdbc:mysql://192.168.18.132:3306/admtokoq_ec0m3rc32";
            String cUser = "root";
            String cPass = "";
            conn = DriverManager.getConnection(cUrl, cUser, cPass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            Log.d("Status: ", "CONNECTED");

        }else{
            Log.d("Status: ", "FAILED");
        }
        return conn;
    }
}

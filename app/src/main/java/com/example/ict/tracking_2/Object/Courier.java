package com.example.ict.tracking_2.Object;

/**
 * Created by ict on 11/30/17.
 */

public class Courier {

    private String id_order;
    private String reference;
    private String firstname;
    private String lastname;
    private String cust_address;
    private String cust_postcode;
    private String cust_city;
    private String cust_phone;
    private String cust_phone_mobile;
    private String product_name;
    private String product_quantity;
    private String id_pelanggan;

    public Courier(String id_order, String reference, String id_pelanggan, String firstname, String lastname,
                   String cust_address, String cust_postcode, String cust_city, String cust_phone,
                   String cust_phone_mobile, String product_name, String product_quantity) {
        super();
        this.id_order = id_order;
        this.reference = reference;
        this.firstname = firstname;
        this.lastname = lastname;
        this.cust_address = cust_address;
        this.cust_postcode = cust_postcode;
        this.cust_city = cust_city;
        this.cust_phone = cust_phone;
        this.cust_phone_mobile = cust_phone_mobile;
        this.product_name = product_name;
        this.product_quantity = product_quantity;
        this.id_pelanggan = id_pelanggan;
    }

    public String getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(String id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getCust_phone_mobile() {
        return cust_phone_mobile;
    }

    public void setCust_phone_mobile(String cust_phone_mobile) {
        this.cust_phone_mobile = cust_phone_mobile;
    }

    public String getCust_phone() {
        return cust_phone;
    }

    public void setCust_phone(String cust_phone) {
        this.cust_phone = cust_phone;
    }

    public String getCust_city() {
        return cust_city;
    }

    public void setCust_city(String cust_city) {
        this.cust_city = cust_city;
    }

    public String getid_order() {

        return id_order;
    }

    public void setid_order(String id_order) {
        this.id_order = id_order;
    }

    public String getreference() {
        return reference;
    }

    public void setreference(String reference) {
        this.reference = reference;
    }

    public String getfirstname() {
        return firstname;
    }

    public void setfirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCust_address() {
        return cust_address;
    }

    public void setCust_address(String cust_address) {
        this.cust_address = cust_address;
    }

    public String getCust_postcode() {
        return cust_postcode;
    }

    public void setCust_postcode(String cust_postcode) {
        this.cust_postcode = cust_postcode;
    }

}
